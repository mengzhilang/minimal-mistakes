---
layout: archive
title: "设计专栏"
permalink: /设计专栏/
author_profile: true

sidebar:
  nav: "docs"
---

{% include base_path %}
{% include group-by-array collection=site.posts field="categories" %}

{% for category in group_names %}
 {% assign posts = group_items[forloop.index0] %}
 <h2 id="{{ category | slugify }}" class="archive__subtitle">{{ category }}</h2>
 {% for post in design %}
  {% include archive-single.html %}
 {% endfor %}
{% endfor %}