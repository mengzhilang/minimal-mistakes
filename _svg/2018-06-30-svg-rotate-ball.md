---
title: "魔幻旋转球"

sidebar:
  nav: "docs"
---
  
<style>
        /*任何css动画仅适合用chrome浏览器观看，其它浏览器将难以用css呈现动画效果*/
  	/*调整运动中心是关键 */
     .ball {
     	-webkit-animation:ball 1s infinite ease;/*快慢交替*/

         }

         @-webkit-keyframes ball{

         0%{ transform: rotate(0deg);
             transform-origin: 37% 38% ; }

         100%{ transform:rotate(360deg);
               transform-origin: 37% 28%;
           }
}
     .ball:hover {
     	-webkit-animation:ball 10s infinite ease;/*快慢交替*/

         }

         @-webkit-keyframes ball{

         0%{ transform: rotate(0deg);
             transform-origin: 39.9% 29.9% ; }

         100%{ transform:rotate(360deg);
               transform-origin: 39.9% 29.9%;
           }
}
  </style>
<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" viewBox="0 0 960 560" xml:space="preserve">
   <g class="ball">

<path style="fill:#00FFFF;" d="M384.351,122.14c0,0-19.01,0.667-19.676,46c0,0-16,19.333-29.334,0
	C335.34,168.14,337.362,120.807,384.351,122.14z"/>   /*blue*/
  
<path style="fill:#FFFF00;" d="M423.799,192.67c0,0,8.792-16.867-30.32-39.797c0,0-8.931-23.453,14.463-25.521
	C407.943,127.352,448.126,152.447,423.799,192.67z"/> /*yellow*/
 
<path style="fill:#FF0000;" d="M340.721,190.753c0,0,8.691,16.919,50-1.766c0,0,24.304,6.255,12.481,26.548
	C403.202,215.535,359.507,233.843,340.721,190.753z"/> /*red*/

<path d="M366.993,137.662c0,0,21.314-3.197,25.723,0.728c0,0,9.119-12.241,18.963-8.658c0.871,0.317-14.588-9.47-30.891-7.308"/> /*blank*/

<path style="fill:#15F908" d="M358.852,199.066c0,0-10.16-19.008-7.941-24.478c0,0-14.59-4.486-14.516-14.961c0.007-0.927-4.029,16.919,3.475,31.553"/> /*green*/

<path style="fill:#FF00FF;" d="M414.607,171.437c0,0-5.008,20.963-10.297,23.583c0,0,7.941,13.037,0.936,20.826
	c-0.62,0.689,14.239-9.986,18.333-25.915"/> /*purple*/
 </g>
</svg>

<h3 align="center" style="font-size:larger">旋转球制作</h3>

<p style="text-indent:2em" >首先要用矢量图编辑软件（比如AI），创建矢量图最后保存为svg格式。非软件创建的矢量图，只有<image>2d图形标签，没有<path>路径</p>
<p style="text-indent:2em">要想制作出几何中心旋转的效果，关键要调整好transform-origin与视框（viewbox）。具体的ransform-origin属性赋值要根据具体情况调试，（center，center）不能产生几何中心旋转的效果。但会存在容器负空间过大的问题，可以仅保留width赋值，减少垂直方向上的负空间。</p>
<p style="text-indent:2em">经过多次尝试，以及参考了老师的范例发现css动画，只能在chrome浏览器渲染出来，其它浏览器看不到动态效果</p>
