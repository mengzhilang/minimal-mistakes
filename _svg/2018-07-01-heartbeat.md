---
title: "怦然心动"

sidebar:
  nav: "docs"
---

<style>
/*请使用chrome浏览器观看*/
    #heart {
	  -webkit-animation:heart 0.8s infinite ease;
	}
	
	@-webkit-keyframes heart{
	  0%{ transform: scale(1);
	     transform-origin:50% 50%;
	  }
	  50%{ transform: scale(1.02);
	       transform-origin:50% 50%;
	  }
	  100%{transform: scale(1);
	       transition-delay: 0.3s;
	       transform-origin:50% 50%;
	  }
	}
	
     #heart:hover {animation-play-state:paused;}

     #heart {
	filter: drop-shadow(8px 8px 6px #333)
	}
</style>
<svg version="1.1" id="heart" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
y="0px" width="100%" height="100%" viewBox="0 0 640 520" style="enable-background:new 0 0 640 480;" xml:space="preserve">
    <g id="heart">
	<path style="fill:#ED211E;stroke:#FF0000;stroke-width:2;stroke-miterlimit:10;" d="M329.082,104.088c0,0-48-109-172-56
	c-4.598,1.965-244,141,172,422c0,0,388-227.5,183-414C507.403,51.832,404.082-7.912,329.082,104.088z"/>
    </g>
</svg>

<h2 style="font-size:larger;" align="center">跳动的心脏制作</h2>
<p text-indent="2em">主要运用了scale动画实现“跳动”的效果，稍微大一点点即可。而且调整好 transform-origin与transition-delay让效果更逼真</p>
<p text-indent="2em">添加了drop-shadow滤镜让“心脏”看起来比较有立体感</p>