---
title:  "教你制作SVG动图"
modified: 2018-07-07T16:03:49-04:00
categories: 
  - 平面设计
tags:
  - SVG动图
sidebar:
  nav: "docs"
---

{% include base_path %}

{% include toc title="目录" %}


## 什么是svg

<p>Svg是Scalable Vector Graphics的简称，中文全称：可缩放矢量图形。而svg在网页中是基于可扩展标记语言（Extensible Markup Language，简称xml）呈现的，
xml、md（mardown）与html（HyperText Markup Language）都属于标记语言的大家庭，所以三者可以兼容与同一个文件中。</p>

<p>与jpg、png这些压缩位图图形格式的文件相比，svg可以通过xml代码呈现在代码中。
矢量图通过几何特性来绘制图形，只靠软件生成，不像位图那样担心图片的分辨率，放大图像而不失真，所以在网页中广泛使用，特别是小图标</p>

<img src="/minimal-mistakes/images/icon.jpg" alt="svg被广泛地应用" style="max-width:100%">

## 怎么制作SVG


<img src="/minimal-mistakes/images/11.png" alt="svg不可能靠敲代码做出来" style="max-width:100%">

<p>这么多个坐标点的代码，自己一个个敲出来？别傻了，直接敲代码毫无概念且繁琐，幸好矢量编辑软件可以帮我们自动生成矢量图的路径代码。
这里我将列举AI（Adobe Illustrator）软件和制作星星例子。</p>

<img src="/minimal-mistakes/images/22.png" alt="svg不可能靠敲代码做出来" style="max-width:100%">

<p>打开AI，使用钢笔工具，在网上寻找图片素材，按照图片素材的轮廓用钢笔工具建立锚点就好了（AI钢笔工具的使用与Photoshop的一样，在此就不细将钢笔工具如何使用了）。
右边控制面板可以调整路径填充色，描边的粗细和颜色。（这里不像Photoshop，有单独的工作路径窗口。AI会边建锚点边自动填色。）</p>

<p>把图画好之后，新建另一个文件，配置如图：（配置文件选为web或自定）</p>

<img src="/minimal-mistakes/images/33.png" alt="用AI来做svg" style="max-width:100%">

<p>
将自己做好的图片剪切到新的文件里，选中图片，文件—》脚本—》将文档储存为“svg”或存储为svg格式。--》svg选项设置，配置如图
</p>

<img src="/minimal-mistakes/images/44.png" alt="用AI来做svg" style="max-width:100%">

做好一份svg啦！

<img src="/minimal-mistakes/images/55.png" alt="用AI来做svg" style="max-width:100%">

注意：**千万不用随便找一张图，将其保存为svg格式来做动画**，否则不会有path或point路径代码，
只会有一大串data（data只是一组canvas的数据，相当于url的作用）难以做出具体的动画效果。

<img src="/minimal-mistakes/images/66.png" alt="不用随便找一张图,保存为svg格式来做动画" style="max-width:100%">

<p>或者实在不会（不想）用钢笔工具构图，可以找rwd（响应式web设计HTML5和css实战）的代码案例。
Chapter 8文件夹有部分svg图的代码案例，直接引用里面的xml代码就好了。</p>

## 为svg添加css动画效果

就拿我自制的“跳动的心脏”来说，

<img src="/minimal-mistakes/images/77.png" alt="用代码做动画" style="max-width:100%">

首先随便命名一个选择器，我使用的是[animation](http://www.w3school.com.cn/cssref/pr_animation.asp)(动画属性)来制作动画。
Animation：动画名称 动画时间 延迟时间 运动变化；（我的动画名称为“heart”、动画要0.8s完成、用来默认值infinite不延迟、变化为ease函数） 
<br>

前面animation的名称一定要与后面的@keyframes的命名一致！[transform](http://www.w3school.com.cn/cssref/pr_transform.asp)（动画属性或transition的赋值，这里就当成动画属性来讲。） scale、rotate、translate、matrix、skew、perspective五种动画效果供君选择。
这里我选择了scale（按比例缩放大小）
<br>

<img src="/minimal-mistakes/images/88.png" alt="用代码做动画">

<p>@keyframes 这里就是设置关键帧。
开始保持原来的大小，每一帧的transform-orgin确保发起动画位置为svg图的几何中心。
第二帧开始是仅扩大了原来的1.02倍，第三帧添加了transition-delay延迟0.3s才开始（为了逼真）。就这样做成了类似于心跳的动画。</p>

<img src="/minimal-mistakes/images/99.png" alt="用代码做动画" style="max-width:100%">

<p>在下面的&it;svg&gt;标签里千万要记得把id改为你所做的动画名称！viewbox将会涉及到svg的位置和大小。建议将width和height改为百分比，实现响应式变化。</p>

<img src="/minimal-mistakes/images/1010.png" alt="用代码做动画" style="max-width:100%">
<br>
&lt;svg&gt;和&lt;g&gt;&lt;path&gt;的代码就是AI帮你生成的。Fill设置填充颜色、stroke设置边框颜色。可根据自己的需要进行调整。

如果实在不会做（不想做），推荐你访问[CodePen](https://codepen.io/search/pens?q=svg%20animation&page=1&order=popularity&depth=everything&show_forks=false)参考、学习借鉴里面的svg动图案例。
<br>
## 将svg动图放到网站上

把图做好之后，那就简单了。你只需要直接复制粘贴到md文档里就可以啦。（前提是内链搭好了，文件命名正确）

<img src="/minimal-mistakes/images/1111.png" alt="直接放到md文档" style="max-width:100%">

大功告成啦！（可惜的是svg动图的支持程度不是很高。）

<img src="/minimal-mistakes/images/1212.png" alt="直接放到md文档" style="max-width:100%">

## 总结

构图用软件，动画靠代码，浏览用chrome。

