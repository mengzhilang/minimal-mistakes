---
title: "搭建完整的内链结构"
modified: 2018-07-08T23:32:41-12:00
categories:
  - 界面设计
tags:
  - 搭内链
sidebar:
  nav: "docs"
---

{% include base_path %}
{% include toc title="目录" %}

> 用jekyll建立完整的内链，是搭建个人博客的关键所在。
所以小编将以关于svg导航为例，详细讲解搭建内链的完整流程。

## 第一步：_pages

<p>首先先新建一个叫名为“_pages”的文件夹，之所以要以_pages命名，
是因为在_config.yml文件里，已经配置好了_pages的文件夹。</p>

<p>然后在_pages文件夹新建md文件——svg.archive
(archive意为“存档”，旨在建立关于svg内容的存档)，写以下代码：</p>

<img src="/minimal-mistakes/images/pages.png" alt="svg.archive" style="max-width:100%;margin:5px;">

<p>在---（分隔线）里面layout确定布局为“archive”（存档即表示用于建内链）；
title就是那个界面的大标题；permalink生成永久链接；author_profile:
只有true or false（在插入header图片的时候，需要将author_profile:
设为true，否则author的信息消失。）</p>

<p>接下来的代码都是用Liquid模板写的，小编能力有限就不细讲了。
% include base_path %  在其他文档里都能见到。
这些liquid语言老老实实复制老师的代码就好了……
（关于其它的导航，也是老老实实复制老师的代码就好了，
但title、permalink是可以根据自己的爱好来设置的，其它部分就不要动了。
复制粘贴后时记得把加号删除，保持原来的对齐格式（空格占位符）。</p>

**Attention: 像自我介绍等直接进入的网页内链，代码如下：**

<img src="/minimal-mistakes/images/attention.png" alt="注意事项" style="max-width:100%;margin:5px;">

## 第二步：_config.yml

<p>复制以下代码至_config.yml文件里。(建立其它导航可以省略这一步操作,因为其它文章只需要放入_post文件夹即可
（用于存放呈现在首页的文件夹）。
如果原来有了_post的部分就不需要重复复制了，只复制关于svg部分的代码即可。)</p>

<img src="/minimal-mistakes/images/config.png" alt="config.yml配置很重要" style="max-width:100%;margin:5px;">

<p>在config.yml里部署collection与default配置。无论breadcrumb导航路径、侧边导航栏、paginate分页等等除了上传文章的改动，
都要在config.yml文件进行修改，
否则直接造成404或无效果。因为config.yml保存整个Jekyll网站配置数据的文件。</p>

## 第三步：navigation.yml	

<p>直接在main下面加入title：svg，navigation.yml的url
要与svg.archive里的permalink相同。
还要注意对齐格式（多一个少一个空格都不行！）
因为yaml语法使用缩进来表示层级关系，只有对齐的元素才算是同一层级的，
否则会导致404问题。（title定义导航名称）</p>

<img src="/minimal-mistakes/images/navigation.png" alt="navigation.yml" style="max-width:100%;margin:5px;">

这样基本的内链就搭好啦！（链接后面加上# title，可在同一个url里有不同的优先呈现顺序）

## 第四步：上传md文章至svg文件夹

<p>新建“_svg”文件夹，每篇文章在分隔线内定一个title（文章标题）就好了。</p>

<img src="/minimal-mistakes/images/svg.png" alt="navigation.yml" style="max-width:100%;margin:5px;">

<p>但要注意每篇文章的文件命名！【年-月-日-文件名称-后缀名（后缀名决定了文件属性，一般我们统一定义为md文件，用md语法写）】。</p>

**Attention：其它导航栏的文章就直接放到_post文件夹就好了。在文章中添加categories（分类）和  tags（标签）有利于更好的对文章分门别类。**

<p>由于md、xml、html都属于标记语言的大家庭（只是科系不同而已），所以在同一个md文件里，你可以同时使用md、xml、html语言来写同一篇文章，但不可以同时用于一个文段上，否则没有效果！</p>

## 第五步：关于建立其它文档的注意事项

<img src="/minimal-mistakes/images/other.png" alt="attention" style="max-width:100%;margin:5px;">

在文章中添加categories（分类）和  tags（标签）有利于更好的对文章分门别类。% include base_path %（不可省略）% include toc title="目录" %（用于根据标题建立文章导航栏）。Modified定义文章更新日期，在网页中会按Modified设置排序。Title定义文章标题。
<br>
最后大功告成啦！

<img src="/minimal-mistakes/images/success0.png" alt="attention" style="max-width:100%;margin:5px;">


<img src="/minimal-mistakes/images/success1.png" alt="attention" style="max-width:100%;margin:5px;">

<img src="/minimal-mistakes/images/success2.png" alt="attention" style="max-width:100%;margin:5px;">

## 总结

<p>Svg导航内链：_pages文件夹(复制老师的代码)->_config.yml文件(复制老师的代码)-> navigation.yml文件（对应着permalink来改）->_svg文件夹（上传md文件）（不在首页呈现）</p>

<p>其余（平面设计、界面设计的导航内链）导航内链：_pages文件夹(复制老师的代码)-> navigation.yml文件（对应着permalink来改）->__post文件夹（上传md文件）（在首页呈现），文章配置catergories 和tags、modified</p>

<p>直接进入的导航内链：_pages文件夹（配置permalink）->_navigation.yml(permalink=url) </p>
