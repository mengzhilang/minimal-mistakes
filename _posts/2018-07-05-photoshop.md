---
title:  "photoshop6种抠图方法"
modified: 2018-07-05T16:03:49-04:00
categories: 
  - 平面设计
tags:
  - Photoshop抠图
sidebar:
  nav: "docs"
---

{% include base_path %}
{% include toc title="目录" %}

## 1. 魔棒、快速选择工具
<p>方法：左边工具栏-->选择魔棒或快速选择工具(记得要选中相应的图层！)</p>
<p>技巧：按住shift加选，alt减选。</p>
<p>适用情况：你想要的部分与其余部分的颜色对比鲜明。</p>

<img src="/minimal-mistakes/images/魔棒.jpg" alt="魔棒工具" style="max-width:100%">

<p>优点：操作简单、容易上手、省时省力。</p>
<p>缺点：背景为纯色的情况适合，在一般的拍摄背景里，难以建立完整选区。
适用情况少；对毛发、风沙等杂碎物体，使用起来并不方便。</p>

## 2. 色彩范围
方法：上方的菜单栏-->点击“选择”-->点击“色彩范围”-->调整容差（容差数值越大，所选择颜色就越多，反之越少。要针对具体情况，调整的容差）

<img src="/minimal-mistakes/images/色彩范围.jpg" alt="色彩范围" style="max-width:100%">

<p>适用范围：你想要的部分与其余部分的颜色对比鲜明。</p>

<p>优点：操作简单、省时省力</p>
<p>缺点：单凭颜色建立选区，可能会选择了你不想要的部分。调整好容差值至关重要。如果软件版本或电脑显卡有问题，在使用Photoshop过程中会强制退出！适用的范围不大。</p>

## 3. 套索工具(磁性套索使用居多)

<p>方法：左边工具栏-->点击“套索工具”（套索工具、多边套索工具、磁性套索工具）</p>
<p>技巧：将图像（同时按住ctrl与+）放大，精准抠图；抠图到了视口边界时，可按住空格键，鼠标变成抓手工具，即可拖动图像；backspace键可删除，前几步建立好的锚点</p>

<img src="/minimal-mistakes/images/套索工具.jpg" alt="套索工具" style="max-width:100%">

<p>优点： 适用范围较广，能满足基本的需求；可以更精确辨识色彩。</p>
<p>缺点：围绕边缘建立选区，较为费时。边缘残留较多（特别是抠证件照）。不适用于对毛发、风沙等杂碎物体抠图。操作较为麻烦。</p>

## 4. 图层蒙版

<p>方法：控制面板点击最底下第三个按键-->选中蒙版-->在工具栏，使用画笔工具（画笔调为黑色或白色，黑色覆盖、白色还原、50%灰色半遮盖）</p>
<p>技巧：蒙版除了用来抠图，还可以调整画笔的流量与不透明度，不同程度地虚化边缘，减少背景与抠图冲突感；英文输入法按住[]键可调整画笔大小。</p>

<img src="/minimal-mistakes/images/蒙版.png" alt="图层蒙版" style="max-width:100%">
<img src="/minimal-mistakes/images/蒙版2.jpg" alt="图层蒙版2" style="max-width:100%">

<p>优点：鼠标操作灵活、透明度可控、不直接作用于原图，不会破坏原图。</p>
<p>缺点：边缘处理难（可能会导致边缘凹凸不平）、费时费力、鼠标控制要求较高。</p>

## 5. 钢笔工具

方法：左边工具栏-->点击“钢笔工具”（其它的工具不怎么用）
技巧：按住ctrl键，即可用鼠标（直接选择工具）调整锚点或曲线的角度、长度；backspace键可以删除之前建立的锚点。每次建立锚点时，不用立即松手，先调整好曲线再松鼠标；后期还可以用直接选择工具调整曲线。

<img src="/minimal-mistakes/images/钢笔工具.jpg" alt="钢笔工具" style="max-width:100%">

<p>优点：不受色彩的影响，自由建立工作路径，适用范围广。鼠标操作灵活。工作路径可保留，方便工作。选取边缘较为平滑。</p>
<p>缺点：要求鼠标操作较为熟练，较为费时费力。不适用于对毛发、风沙等杂碎物体抠图。</p>

## 6. 色彩通道

<p>方法：右边控制面板-->取消通道全选，选择一个黑白对比度最强烈的通道（尽量少的噪点）-->复制该通道-->用曲线或色阶（控制面板的调整或菜单栏的图像调整），进一步增强黑白对比度。-->将画笔工具的颜色（前景色）换成白色-->涂白自己所需要的部分-->建立选区（可用魔棒），选择原来的图层ctrl+J即可从原图抠下来。</p>
<p>技巧：还可以使用alpha通道（透明度）抠图，选择存储选区，所建立通道即为alpha通道，方法同上。</p>

<img src="/minimal-mistakes/images/色彩通道.jpg" alt="色彩通道" style="max-width:100%">

<p>优点：无论抠什么图像都可以适用，针对毛发、流沙等杂碎实物进行精细抠图。</p>
<p>缺点：技术要求高、处理不当容易失去某些色彩通道，破坏图像。</p>
<p>没有最好的抠图方法，只有最合适的抠图方法，根据图像的具体情况选择不同的方法工具进行抠图，平时多点运用不同的方法抠图，才能高效、高质量地抠图。</p>



